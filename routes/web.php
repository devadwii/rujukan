<?php

use App\Http\Controllers\ApprovalController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\RujukanKeluarController;
use App\Http\Controllers\RujukanMasukController;
use App\Http\Controllers\BalasanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthController::class, 'showFormLogin']);
Route::get('login', [AuthController::class, 'showFormLogin'])->name('login');
Route::post('login', [AuthController::class, 'login']);
Route::get('register', [AuthController::class, 'showFormRegister'])->name('register');
Route::post('register', [AuthController::class, 'register']);

Route::group(['middleware' => 'auth'], function () {
    Route::get('home', [HomeController::class, 'index'])->name('home');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
});

Route::get('getNumberApproval', [HomeController::class, 'getNumberApproval'])->name('getNumberApproval');

Route::resource('doctors', DoctorController::class);
Route::get('doctors/{id}/download',[DoctorController::class, 'download'])->name('doctors.download');
Route::get('editProfile',[DoctorController::class, 'editProfile'])->name('editProfile');
Route::get('ambilJamRujukan', [DoctorController::class, 'ambilJamRujukan']);
Route::post('updateProfil', [DoctorController::class, 'updateProfil'])->name('updateProfil');
Route::get('inactiveDoctor', [DoctorController::class, 'inactiveDoctor'])->name('inactiveDoctor');

Route::resource('approvals', ApprovalController::class);
Route::get('approveDoctor', [ApprovalController::class, 'approveDoctor'])->name('approveDoctor');

Route::resource('rujukanKeluar', RujukanKeluarController::class);
Route::get('createRujukan',[RujukanKeluarController::class, 'createRujukan'])->name('createRujukan');

Route::resource('rujukanMasuk', RujukanMasukController::class);

Route::resource('balasan', BalasanController::class);
Route::get('viewBalasan', [BalasanController::class, 'viewBalasan'])->name('viewBalasan');

Route::get('send-email','App\Http\Controllers\MailController@sendEmail');
