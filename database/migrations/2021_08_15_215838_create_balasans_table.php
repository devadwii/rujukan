<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBalasansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balasans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_rujukan', 250)->nullable();
            $table->date('tanggal')->nullable();
            $table->mediumText('konsul_selesai')->nullable();
            $table->mediumText('perlu_kontrol_kembali')->nullable();
            $table->mediumText('perlu_kontrol_ke_ahli')->nullable();
            $table->mediumText('perlu_dirawat')->nullable();
            $table->mediumText('hasil_pemeriksaan')->nullable();
            $table->mediumText('diagnosa')->nullable();
            $table->mediumText('perawatan_yang_sudah_dilakukan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balasans');
    }
}
