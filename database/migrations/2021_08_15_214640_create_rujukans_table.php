<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRujukansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rujukans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jenis_rujukan', 250)->nullable();
            $table->string('tanggal', 250)->nullable();
            $table->string('jam_rujuk', 250)->nullable();
            $table->string('id_dokter_perujuk', 250)->nullable();
            $table->string('id_dokter_rujukan', 250)->nullable();
            $table->string('no_registrasi', 250)->nullable();
            $table->string('nama_pasien', 250)->nullable();
            $table->string('jenis_kelamin', 250)->nullable();
            $table->string('umur', 250)->nullable();
            $table->string('no_telepon', 250)->nullable();
            $table->mediumText('alamat')->nullable();
            $table->mediumText('keluhan')->nullable();
            $table->mediumText('pemeriksaan_klinis')->nullable();
            $table->mediumText('pemeriksaan_lab')->nullable();
            $table->mediumText('diagnosa_sementara')->nullable();
            $table->mediumText('terapi')->nullable();
            $table->string('status', 100)->nullable();
            $table->string('id_balasan', 250)->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rujukans');
    }
}
