<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id', 250);
            $table->string('no_hp', 250)->nullable();
            $table->string('gelar', 250)->nullable();
            $table->string('deskripsi', 250)->nullable();
            $table->string('tempat_praktik', 250)->nullable();
            $table->string('jam_buka_praktek', 250)->nullable();
            $table->string('jam_tutup_praktek', 250)->nullable();
            $table->string('no_sip', 250)->nullable();
            $table->integer('harga_konsultasi')->nullable();
            $table->string('avatar_url', 250)->default('default.jpg');
            $table->string('data_url', 250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
