<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJadwalPrakteksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_prakteks', function (Blueprint $table) {
            $table->id();
            $table->string('user_id', 250);
            $table->string('senin_buka', 10)->nullable();
            $table->string('senin_tutup', 10)->nullable();
            $table->string('selasa_buka', 10)->nullable();
            $table->string('selasa_tutup', 10)->nullable();
            $table->string('rabu_buka', 10)->nullable();
            $table->string('rabu_tutup', 10)->nullable();
            $table->string('kamis_buka', 10)->nullable();
            $table->string('kamis_tutup', 10)->nullable();
            $table->string('jumat_buka', 10)->nullable();
            $table->string('jumat_tutup', 10)->nullable();
            $table->string('sabtu_buka', 10)->nullable();
            $table->string('sabtu_tutup', 10)->nullable();
            $table->string('minggu_buka', 10)->nullable();
            $table->string('minggu_tutup', 10)->nullable();
            $table->string('keterangan_praktek', 250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal_prakteks');
    }
}
