<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use App\Models\JadwalPrakteks;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use DataTables;


class DoctorController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $user = Auth::user();
        $currentUser = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*',
            )
            ->where('users.id','=',$user->id)
            ->first();

        $doctors = DB::table('doctors')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.complete_data',
                'users.name',
                'doctors.gelar',
                'doctors.tempat_praktik'
            )
            ->where('gelar','<>','Admin')
            ->where('status','=','Approved')
            ->where('users.id','<>',$user->id)
            ->where('users.remember_token','=',NULL)
            ->get();

        if ($request->ajax()) {

            if ($currentUser->gelar == "Admin") {
                return Datatables::of($doctors)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="View" class=" btn btn-primary btn-sm viewDoctor">View</a>';

                        $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Inactive" class="btn btn-danger btn-sm inactiveDoctor">Inactive</a>';

                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            } else {
                return Datatables::of($doctors)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="View" class=" btn btn-primary btn-sm viewDoctor">View</a>';

                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }

        }
        return view('listDoctor',compact( 'currentUser'));
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        $photo = $request->url_photo;
        $pdf = $request->url_dokumen;

        if ($photo != null) {
            $photo_profil = 'Profil - '.$user->name.'.'.$request->foto_profil->extension();
            $request->foto_profil->move(public_path('fotoProfil'), $photo_profil);
        } else {
            $photo_profil = $request->url_photo;
        }

        if ($pdf != null) {
            $sertifikat = 'Sertifikat - '.$user->name.'.'.$request->dokumen_sertifikat->extension();
            $request->dokumen_sertifikat->move(public_path('sertifikat'), $sertifikat);
        } else {
            $sertifikat = $request->url_dokumen;
        }

        $rules = [
            'no_hp'             => 'required',
            'gelar'             => 'required',
            'tempat_praktik'    => 'required',
            'no_sip'            => 'required',
            'harga_konsultasi'  => 'required',
            'url_photo'         => 'required',
            'url_dokumen'       => 'required'
        ];

        $messages = [
            'no_hp.required'            => 'Masukkan No HP.',
            'gelar.required'            => 'Masukkan Gelar.',
            'tempat_praktik.required'   => 'Masukkan Tempat Praktik',
            'no_sip.required'           => 'Masukkan No SIP.',
            'harga_konsultasi.required' => 'Masukkan Harga Konsultasi.',
            'url_photo.required'       => 'Masukkan Data Foto Profil.',
            'url_dokumen.required'         => 'Masukkan Data Sertifikat'
        ];


        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            return response()->json(['error'=>'Failed to add data.', 'errors'=>$validator->errors()]);
        }

        Doctor::updateOrCreate(
            ['id' => $request->id],
            [
                'user_id' => $user->id,
                'no_hp' => $request->no_hp,
                'gelar' => $request->gelar,
                'deskripsi' => $request->deskripsi,
                'tempat_praktik' => $request->tempat_praktik,
                'no_sip' => $request->no_sip,
                'harga_konsultasi' => $request->harga_konsultasi,
                'avatar_url' => $photo_profil,
                'data_url' => $sertifikat,
            ]
        );

        JadwalPrakteks::Create(
            [
                'user_id' => $user->id,
                'senin_buka' => '00:00',
                'senin_tutup' => '00:00',
                'selasa_buka' => '00:00',
                'selasa_tutup' => '00:00',
                'rabu_buka' => '00:00',
                'rabu_tutup' => '00:00',
                'kamis_buka' => '00:00',
                'kamis_tutup' => '00:00',
                'jumat_buka' => '00:00',
                'jumat_tutup' => '00:00',
                'sabtu_buka' => '00:00',
                'sabtu_tutup' => '00:00',
                'minggu_buka' => '00:00',
                'minggu_tutup' => '00:00',
                'keterangan_praktek' => '',
            ]
        );

        $updateDoctor = DB::table('users')
            ->where('id', $user->id)
            ->update(['complete_data' => "Y"]);

        return response()->json(['success'=>'Doctor saved successfully.']);
    }

    public function updateProfil(Request $request)
    {
        $user = Auth::user();

        $rules = [
            'email'             => 'required',
            'no_hp'             => 'required',
            'gelar'             => 'required',
            'tempat_praktik'    => 'required',
            'no_sip'            => 'required',
            'harga_konsultasi'  => 'required'
        ];

        $messages = [
            'email.required'            => 'Email Tidak Boleh Kosong',
            'no_hp.required'            => 'No HP. Tidak Boleh Kosong',
            'gelar.required'            => 'Gelar Tidak Boleh Kosong',
            'tempat_praktik.required'   => 'Tempat Praktik Tidak Boleh Kosong',
            'no_sip.required'           => 'No SIP. Tidak Boleh Kosong',
            'harga_konsultasi.required' => 'Harga Konsultasi Tidak Boleh Kosong'
        ];


        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            return response()->json(['error'=>'Failed to update data.', 'errors'=>$validator->errors()]);
        }

        Doctor::updateOrCreate(
            ['id' => $request->doktor_id],
            [
                'user_id' => $user->id,
                'no_hp' => $request->no_hp,
                'gelar' => $request->gelar,
                'deskripsi' => $request->deskripsi,
                'tempat_praktik' => $request->tempat_praktik,
                'jam_buka_praktek' => $request->jam_buka_praktek_input,
                'jam_tutup_praktek' => $request->jam_tutup_praktek_input,
                'no_sip' => $request->no_sip,
                'harga_konsultasi' => $request->harga_konsultasi,
            ]
        );

        User::updateOrCreate(
            ['id' => $user->id],
            [
                'email' => $request->email,
            ]
        );


        JadwalPrakteks::updateOrCreate(
            ['user_id' => $user->id],
            [
                'senin_buka' => $request->senin_buka_input,
                'senin_tutup' => $request->senin_tutup_input,
                'selasa_buka' => $request->selasa_buka_input,
                'selasa_tutup' => $request->selasa_tutup_input,
                'rabu_buka' => $request->rabu_buka_input,
                'rabu_tutup' => $request->rabu_tutup_input,
                'kamis_buka' => $request->kamis_buka_input,
                'kamis_tutup' => $request->kamis_tutup_input,
                'jumat_buka' => $request->jumat_buka_input,
                'jumat_tutup' => $request->jumat_tutup_input,
                'sabtu_buka' => $request->sabtu_buka_input,
                'sabtu_tutup' => $request->sabtu_tutup_input,
                'minggu_buka' => $request->minggu_buka_input,
                'minggu_tutup' => $request->minggu_tutup_input,
                'keterangan_praktek' => $request->keterangan_praktek,
            ]
        );

        return response()->json(['success'=>'Doctor saved successfully.']);
    }


    public function download($id)
    {
        $doctor = Doctor::find($id);
        $file = public_path()."/sertifikat/".$doctor->data_url;
        return response()->download($file);
    }

    public function ambilJamRujukan(Request $request)
    {
        $id = $request->idDokter;

        $doctor = DB::table('doctors')
            ->select(
                'doctors.jam_buka_praktek',
                'doctors.jam_tutup_praktek',
            )
            ->where('doctors.id','=',$id)
            ->first();


        $timestamp = strtotime($request->hariRujukan);
        $day = strtoupper(date('D', $timestamp));

        $jadwalPraktek = DB::table('jadwal_prakteks')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'jadwal_prakteks.user_id');
            })
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'jadwal_prakteks.*'
            )
            ->where('doctors.id','=',$id)
            ->first();

        $jamRujukan = [];

        switch ($day) {
            case "SUN":
                $jamRujukan = array("jam_buka_praktek" => $jadwalPraktek->minggu_buka, "jam_tutup_praktek" =>$jadwalPraktek->minggu_tutup);
                break;
            case "MON":
                $jamRujukan = array("jam_buka_praktek" => $jadwalPraktek->senin_buka, "jam_tutup_praktek" =>$jadwalPraktek->senin_tutup);
                break;
            case "TUE":
                $jamRujukan = array("jam_buka_praktek" => $jadwalPraktek->selasa_buka, "jam_tutup_praktek" =>$jadwalPraktek->selasa_tutup);
                break;
            case "WED":
                $jamRujukan = array("jam_buka_praktek" => $jadwalPraktek->rabu_buka, "jam_tutup_praktek" =>$jadwalPraktek->rabu_tutup);
                break;
            case "THU":
                $jamRujukan = array("jam_buka_praktek" => $jadwalPraktek->kamis_buka, "jam_tutup_praktek" =>$jadwalPraktek->kamis_tutup);
                break;
            case "FRI":
                $jamRujukan = array("jam_buka_praktek" => $jadwalPraktek->jumat_buka, "jam_tutup_praktek" =>$jadwalPraktek->jumat_tutup);
                break;
            case "SAT":
                $jamRujukan = array("jam_buka_praktek" => $jadwalPraktek->sabtu_buka, "jam_tutup_praktek" =>$jadwalPraktek->sabtu_tutup);
                break;
            default:
        }

        $jadwal = array("keterangan_praktek" =>$jadwalPraktek->keterangan_praktek);

//        return response()->json(['jamRujukan'=>$doctor]);
        return response()->json(['jamRujukan'=>$jamRujukan,'jadwal'=>$jadwal]);
    }

    public function edit($id)
    {
        $user = Auth::user();
        $currentUser = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*',
            )
            ->where('users.id','=',$user->id)
            ->first();

        $doctor = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*',
            )
            ->where('users.id','=',$id)
            ->first();

        $jadwal = DB::table('jadwal_prakteks')
            ->select('*')
            ->where('user_id','=',$id)
            ->first();

        return view('profilViewer',
            compact('currentUser','doctor','jadwal'));
    }


    public static function editProfile()
    {
        $user = Auth::user();
        $currentUser = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*',
            )
            ->where('users.id','=',$user->id)
            ->first();

        $doctor = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.id AS doctor_id',
                'doctors.*',
            )
            ->where('users.id','=',$user->id)
            ->first();

        $jadwal = DB::table('jadwal_prakteks')
            ->select('*')
            ->where('user_id','=',$user->id)
            ->first();

        return view('profil',
            compact('currentUser','doctor','jadwal'));
    }

    public static function inactiveDoctor(Request $request)
    {
        $affected = DB::table('users')
            ->where('id', $request->user_id)
            ->update(['remember_token' => "Inactive"]);

        return response()->json(['success'=>'Inactive doctor successfully.']);
    }

}
