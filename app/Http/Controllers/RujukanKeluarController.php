<?php

namespace App\Http\Controllers;

use App\Models\Rujukan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use DataTables;
use Validator;

class RujukanKeluarController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $user = Auth::user();
        $currentUser = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*',
            )
            ->where('users.id','=',$user->id)
            ->first();

        $rujukans = DB::table('rujukans')
            ->leftJoin('doctors', function ($join) {
                $join->on('doctors.id', '=', 'rujukans.id_dokter_rujukan');
            })
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'rujukans.id',
                'tanggal',
                'id_dokter_rujukan',
                'name',
                'nama_pasien',
            );



        if($currentUser->gelar == "Admin") {
            $rujukans = $rujukans->where('rujukans.status','=','Menunggu Balasan')->get();
        } else {
            $rujukans = $rujukans->where('rujukans.status','=','Menunggu Balasan')->where('id_dokter_perujuk','=',$currentUser->id)->get();
        }

        if ($request->ajax()) {
            return Datatables::of($rujukans)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editRujukan">Lihat Detail</a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('listRujukanKeluar',compact('rujukans','currentUser'));
    }


    public static function createRujukan()
    {
        $user = Auth::user();
        $currentUser = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*',
            )
            ->where('users.id','=',$user->id)
            ->first();

        $docterRujukan = DB::table('doctors')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*'
            )
            ->where('gelar','like','%Spesialis')
            ->where('status','=','Approved')
            ->where('users.id','<>',$user->id)
            ->get();

        $nomorRegistrasi = Rujukan::nomorRegistrasi();

        if ($currentUser->gelar == "Dokter Gigi") {
            return view('createRujukanGigi',
                compact('currentUser','docterRujukan','nomorRegistrasi'));
        } else {
            return view('createRujukanUmum',
                compact('currentUser','docterRujukan','nomorRegistrasi'));
        }
    }

    public function store(Request $request)
    {
        $rules = [
            'tanggal'               => 'required',
            'jam_merujuk_input'     => 'required',
            'id_dokter_rujukan'     => 'required',
            'nama_pasien'           => 'required',
            'jenis_kelamin'         => 'required',
            'umur'                  => 'required',
            'no_telepon'            => 'required',
            'alamat'                => 'required'
        ];

        $messages = [
            'tanggal.required'              => 'Masukkan Tanggal Rujukan.',
            'jam_merujuk_input.required'    => 'Tentukan Jam Rujukan',
            'id_dokter_rujukan.required'    => 'Tentukan Dokter Rujukan',
            'nama_pasien.required'          => 'Masukkan Nama Pasien',
            'jenis_kelamin.required'        => 'Masukkan Jenis Kelamin Pasien',
            'umur.required'                 => 'Masukkan Umur Pasien',
            'no_telepon.required'           => 'Masukkan No Telepon Pasien',
            'alamat.required'               => 'Masukkan Alamat Pasien'
        ];


        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            return response()->json(['error'=>'Failed to add data.', 'errors'=>$validator->errors()]);
        }

        $rujukan = Rujukan::updateOrCreate(
            ['id' => $request->id_rujukan],
            [
                'jenis_rujukan' => $request->jenis_rujukan,
                'tanggal' => $request->tanggal,
                'jam_rujuk' => $request->jam_merujuk_input,
                'id_dokter_perujuk' => $request->id_dokter_perujuk,
                'id_dokter_rujukan' => $request->id_dokter_rujukan,
                'no_registrasi' => $request->no_registrasi,
                'nama_pasien' => $request->nama_pasien,
                'jenis_kelamin' => $request->jenis_kelamin,
                'umur' => $request->umur,
                'no_telepon' => $request->no_telepon,
                'alamat' => $request->alamat,
                'keluhan' => $request->keluhan,
                'pemeriksaan_klinis' => $request->pemeriksaan_klinis,
                'pemeriksaan_lab' => $request->pemeriksaan_lab,
                'diagnosa_sementara' => $request->diagnosa_sementara,
                'terapi' => $request->terapi,
                'status' => "Menunggu Balasan",
            ]
        );

        MailController::sendEmail($rujukan);

        return response()->json(['success'=>'Rujukan saved successfully.']);
    }

    public function edit($id)
    {
        $user = Auth::user();
        $currentUser = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*',
            )
            ->where('users.id','=',$user->id)
            ->first();

        $docterRujukan = DB::table('doctors')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*'
            )
            ->where('gelar','like','%Spesialis')
            ->where('status','=','Approved')
            ->get();

        $rujukan = DB::table('rujukans')
            ->where('id','=',$id)
            ->first();

        $dokterPerujuk = DB::table('doctors')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name'
            )
            ->where('doctors.id','=',$rujukan->id_dokter_perujuk)
            ->first();

        if ($rujukan->jenis_rujukan == "Dokter Gigi") {
            return view('viewRujukanGigi',
                compact('currentUser','rujukan','docterRujukan','dokterPerujuk'));
        } else {
            return view('viewRujukanUmum',
                compact('currentUser','rujukan','docterRujukan','dokterPerujuk'));
        }
    }
}
