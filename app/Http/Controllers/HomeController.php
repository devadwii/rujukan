<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApprovalController;
use Illuminate\Support\Facades\Log;


class HomeController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $currentUser = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*',
            )
            ->where('users.id','=',$user->id)
            ->first();

        if($user->status != 'Waiting' && $user->complete_data == 'Y' && $user->remember_token == 'Inactive'){
            return view('inactive',compact('currentUser') );
        } else if ($user->status != 'Waiting' && $user->complete_data == 'Y' )  {
            return view('landing',compact( 'currentUser'));
        } else if ($user->complete_data != 'Y')  {
            return view('addon',compact( 'user'));
        } else if ($user->status == 'Waiting' && $user->complete_data == 'Y') {
            return view('waiting',compact('currentUser') );
        }
    }

    public static function getNumberApproval()
    {
        $jumlahApproval = DB::table('doctors')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(DB::raw('COUNT(*) as count'))
            ->where('gelar','<>','Admin')
            ->where('status','<>','Approved')
            ->first();

        return response()->json(['data'=>$jumlahApproval->count]);
    }
}
