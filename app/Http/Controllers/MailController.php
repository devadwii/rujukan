<?php

namespace App\Http\Controllers;

use App\Models\Rujukan;
use Illuminate\Http\Request;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{

    public function index(){

        $details = [
            'title' => 'Mail from websitepercobaan.com',
            'body' => 'This is for testing email using smtp'
        ];

        \Mail::to('devaads2@gmail.com')->send(new \App\Mail\MyTestMail($details));

        dd("Email sudah terkirim.");

    }

    public static function sendEmail(Rujukan $rujukan){
        $dokterTujuan = DB::table('doctors')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })->select(
                'users.name',
                'users.email',
                'doctors.id'
            )->where('doctors.id','=',$rujukan->id_dokter_rujukan)
            ->first();

        $dokterPengirim = DB::table('doctors')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })->select(
                'users.name',
                'users.email',
                'doctors.id'
            )->where('doctors.id','=',$rujukan->id_dokter_perujuk)
            ->first();

        if($rujukan->jenis_kelamin == "1") {
            $jenis_kelamin = 'Laki-Laki';
        } else {
            $jenis_kelamin = 'Perempuan';
        }

        if($rujukan->status == "Menunggu Balasan") {
            $jenis_email = "Surat Rujukan";
            $dokter_tujuan = $dokterTujuan->name; //kalo surat rujukan, dokter tujuan = spesialis
            $dokter_pengirim = $dokterPengirim->name; // kalo surat rujukan, pemgirimnya = umum / gigi
            $url = 'http://merujuk.com/rujukanKeluar/'.$rujukan->id.'/edit';
            $email_to = $dokterTujuan->email;
        } else {
            $jenis_email = "Surat Balasan";
            $dokter_tujuan = $dokterPengirim->name; //kalo surat balasan, dokter tujuan = umum / gigi
            $dokter_pengirim = $dokterTujuan->name; // kalo surat rujukan, pemgirimnya = spesialis
            $url = 'http://merujuk.com/viewBalasan?id_rujukan='.$rujukan->id;
            $email_to = $dokterPengirim->email;
        }

        $details = [
            'jenis_email' => $jenis_email,
            'dokter_tujuan' => $dokter_tujuan,
            'dokter_pengirim' => $dokter_pengirim,
            'nomor_registrasi' => $rujukan->no_registrasi,
            'tanggal' => $rujukan->tanggal,
            'jam_rujuk' => $rujukan->jam_rujuk,
            'nama_pasien' => $rujukan->nama_pasien,
            'jenis_kelamin_pasien' => $jenis_kelamin,
            'umur' => $rujukan->umur,
            'no_telepon' => $rujukan->no_telepon,
            'url_detail' => $url
        ];

        \Mail::to($email_to)->send(new \App\Mail\MyTestMail($details));

    }
}
