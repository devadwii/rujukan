<?php

namespace App\Http\Controllers;

use App\Models\Rujukan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Log;

class RujukanMasukController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $user = Auth::user();
        $currentUser = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*',
            )
            ->where('users.id','=',$user->id)
            ->first();

        $rujukans = DB::table('rujukans')
            ->leftJoin('doctors', function ($join) {
                $join->on('doctors.id', '=', 'rujukans.id_dokter_perujuk');
            })
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'rujukans.id',
                'tanggal',
                'id_dokter_perujuk',
                'name',
                'nama_pasien',
            );

        if($currentUser->gelar == "Admin") {
            $rujukans = $rujukans->where('rujukans.status','=','Menunggu Balasan')->get();
            if ($request->ajax()) {
                return Datatables::of($rujukans)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm lihatRujukan">Lihat Detail</a>';

                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }
        } else {
            $rujukans = $rujukans->where('rujukans.status','=','Menunggu Balasan')->where('id_dokter_rujukan','=',$currentUser->id)->get();
            if ($request->ajax()) {
                return Datatables::of($rujukans)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm balasRujukan">Buat Balasan</a>';

                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }
        }

        return view('listRujukanMasuk',compact('rujukans','currentUser'));
    }

    public function edit($id)
    {
        $user = Auth::user();
        $currentUser = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*',
            )
            ->where('users.id','=',$user->id)
            ->first();

        $docterRujukan = DB::table('doctors')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*'
            )
            ->where('gelar','like','%Spesialis')
            ->where('status','=','Approved')
            ->get();

        $rujukan = DB::table('rujukans')
            ->where('id','=',$id)
            ->first();

        $dokterPerujuk = DB::table('doctors')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })->select(
                'users.id',
                'users.name',
                'doctors.id'
            )->where('doctors.id','=',$rujukan->id_dokter_perujuk)
            ->first();

        return view('createBalasan',
            compact('currentUser','docterRujukan','rujukan','dokterPerujuk'));

    }


}
