<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class ApprovalController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $user = Auth::user();
        $currentUser = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*',
            )
            ->where('users.id','=',$user->id)
            ->first();

        $doctors = DB::table('doctors')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.complete_data',
                'users.name',
                'doctors.gelar',
                'doctors.tempat_praktik'
            )
            ->where('gelar','<>','Admin')
            ->where('status','<>','Approved')
            ->get();

        if ($request->ajax()) {
            return Datatables::of($doctors)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="View" class=" btn btn-primary btn-sm viewDoctor">View</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Approve" class="btn btn-success btn-sm approveDoctor">Approve</a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('listApproval',compact( 'currentUser'));
    }

    public static function approveDoctor(Request $request)
    {
        $affected = DB::table('users')
            ->where('id', $request->user_id)
            ->update(['status' => "Approved"]);

        return response()->json(['success'=>'Approve successfully.']);
    }

    public function edit($id)
    {
        $user = Auth::user();
        $currentUser = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*',
            )
            ->where('users.id','=',$user->id)
            ->first();

        $doctor = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id AS user_id',
                'users.name',
                'users.email',
                'doctors.*',
            )
            ->where('users.id','=',$id)
            ->first();

        $jadwal = DB::table('jadwal_prakteks')
            ->select('*')
            ->where('user_id','=',$id)
            ->first();


        return view('profilViewerApprove', compact('currentUser','doctor','jadwal'));
    }



}
