<?php

namespace App\Http\Controllers;

use App\Models\Balasan;
use App\Models\Rujukan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Log;
use Validator;

class BalasanController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $user = Auth::user();
        $currentUser = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*',
            )
            ->where('users.id', '=', $user->id)
            ->first();


        if($currentUser->gelar == "Dokter Gigi" || $currentUser->gelar == "Dokter Umum") {
            $rujukans = DB::table('rujukans')
                ->leftJoin('doctors', function ($join) {
                    $join->on('doctors.id', '=', 'rujukans.id_dokter_rujukan');
                })
                ->leftJoin('users', function ($join) {
                    $join->on('users.id', '=', 'doctors.user_id');
                })
                ->select(
                    'rujukans.id',
                    'tanggal',
                    'id_dokter_perujuk',
                    'name',
                    'nama_pasien',
                );;
        } else {
            $rujukans = DB::table('rujukans')
                ->leftJoin('doctors', function ($join) {
                    $join->on('doctors.id', '=', 'rujukans.id_dokter_perujuk');
                })
                ->leftJoin('users', function ($join) {
                    $join->on('users.id', '=', 'doctors.user_id');
                })
                ->select(
                    'rujukans.id',
                    'tanggal',
                    'id_dokter_perujuk',
                    'name',
                    'nama_pasien',
                );;
        }



        if ($currentUser->gelar == "Admin") {
            $rujukans = $rujukans->where('rujukans.status', '=', 'Sudah Dibalas')->get();
        } else {
            if($currentUser->gelar == "Dokter Gigi" || $currentUser->gelar == "Dokter Umum") {
                $rujukans = $rujukans->where('rujukans.status', '=', 'Sudah Dibalas')->where('id_dokter_perujuk', '=', $currentUser->id)->get();
            } else {
                $rujukans = $rujukans->where('rujukans.status', '=', 'Sudah Dibalas')->where('id_dokter_rujukan', '=', $currentUser->id)->get();
            }

        }

        Log::info($rujukans);
        if ($request->ajax()) {
            return Datatables::of($rujukans)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $row->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm lihatBalasan">Lihat Balasan</a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }


        return view('listBalasan', compact('rujukans', 'currentUser'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'tanggal_balasan'               => 'required',
        ];

        $messages = [
            'tanggal_balasan.required'              => 'Masukkan Tanggal Balasan.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            return response()->json(['error'=>'Failed to add data.', 'errors'=>$validator->errors()]);
        }

        $balasan = Balasan::updateOrCreate(
            ['id' => $request->id_balasan],
            [
                'id_rujukan' => $request->id_rujukan,
                'tanggal' => $request->tanggal_balasan,
                'konsul_selesai' => $request->konsul_selesai,
                'perlu_kontrol_kembali' => $request->perlu_kontrol_kembali,
                'perlu_kontrol_ke_ahli' => $request->perlu_kontrol_ke_ahli,
                'perlu_dirawat' => $request->perlu_dirawat,
                'hasil_pemeriksaan' => $request->hasil_pemeriksaan,
                'diagnosa' => $request->diagnosa,
                'perawatan_yang_sudah_dilakukan' => $request->perawatan_yang_sudah_dilakukan,
            ]
        );

        Rujukan::updateOrCreate(
            ['id' => $request->id_rujukan],
            [
                'status' => "Sudah Dibalas",
                'id_balasan' => $balasan->id,
            ]
        );

        $rujukan = Rujukan::find($request->id_rujukan);

        MailController::sendEmail($rujukan);

        return response()->json(['success'=>'Rujukan saved successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Balasan  $balasan
     * @return \Illuminate\Http\Response
     */
    public function show(Balasan $balasan)
    {
        //
    }

    public function edit($id)
    {
        $user = Auth::user();
        $currentUser = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*',
            )
            ->where('users.id','=',$user->id)
            ->first();

        $docterRujukan = DB::table('doctors')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*'
            )
            ->where('gelar','like','%Spesialis')
            ->where('status','=','Approved')
            ->get();

        $rujukan = DB::table('rujukans')
            ->where('id','=',$id)
            ->first();

        return view('createBalasan',
            compact('currentUser','docterRujukan','rujukan'));

    }

    public function viewBalasan(Request $request) {
        $user = Auth::user();
        $currentUser = DB::table('users')
            ->leftJoin('doctors', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*',
            )
            ->where('users.id','=',$user->id)
            ->first();

        $docterRujukan = DB::table('doctors')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name',
                'users.email',
                'doctors.*'
            )
            ->where('gelar','like','%Spesialis')
            ->where('status','=','Approved')
            ->get();

        $rujukan = DB::table('rujukans')
            ->where('id','=',$request->id_rujukan)
            ->first();

        $balasan = DB::table('balasans')
            ->where('id_rujukan','=',$request->id_rujukan)
            ->first();

        $dokterPerujuk = DB::table('doctors')
            ->leftJoin('users', function ($join) {
                $join->on('users.id', '=', 'doctors.user_id');
            })
            ->select(
                'users.id',
                'users.name'
            )
            ->where('doctors.id','=',$rujukan->id_dokter_perujuk)
            ->first();

        return view('viewBalasan',
            compact('currentUser','docterRujukan','rujukan','balasan','dokterPerujuk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Balasan  $balasan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Balasan $balasan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Balasan  $balasan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Balasan $balasan)
    {
        //
    }
}
