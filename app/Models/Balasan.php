<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Balasan extends Model
{
    use HasFactory;
    protected $fillable = ['id_rujukan','tanggal','konsul_selesai','perlu_kontrol_kembali','perlu_kontrol_ke_ahli','perlu_dirawat','hasil_pemeriksaan','diagnosa','perawatan_yang_sudah_dilakukan'];
}
