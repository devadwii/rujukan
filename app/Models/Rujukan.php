<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Rujukan extends Model
{
    use HasFactory;

    protected $fillable = ['jenis_rujukan','tanggal','jam_rujuk','id_dokter_perujuk','id_dokter_rujukan','no_registrasi','nama_pasien',
        'jenis_kelamin','umur','no_telepon','alamat','keluhan','pemeriksaan_klinis','pemeriksaan_lab','diagnosa_sementara','terapi','status','id_balasan'];

    public static function nomorRegistrasi()
    {
        $kode = DB::table('rujukans')->max('id');
        $addNol = '';
        $kode = str_replace("RJKN-", "", $kode);
        $kode = (int) $kode + 1;
        $incrementKode = $kode;

        if (strlen($kode) == 1) {
            $addNol = "0000";
        } elseif (strlen($kode) == 2) {
            $addNol = "000";
        } elseif (strlen($kode == 3)) {
            $addNol = "00";
        }elseif (strlen($kode == 4)) {
            $addNol = "0";
        }

        $kodeBaru = "RJK-".$addNol.$incrementKode;
        return $kodeBaru;
    }
}
