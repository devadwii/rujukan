<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JadwalPrakteks extends Model
{
    use HasFactory;
    protected $fillable = ['user_id','senin_buka','senin_tutup','selasa_buka','selasa_tutup',
        'rabu_buka','rabu_tutup','kamis_buka','kamis_tutup','jumat_buka','jumat_tutup',
        'sabtu_buka','sabtu_tutup','minggu_buka','minggu_tutup','keterangan_praktek'];
}
