<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    use HasFactory;
    protected $fillable = ['user_id','no_hp','gelar','deskripsi','tempat_praktik','jam_buka_praktek','jam_tutup_praktek','no_sip','harga_konsultasi','avatar_url','data_url'];
}
