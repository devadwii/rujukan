<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>MERUJUK</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset('images/logo/logo-kecil.png')}}" >
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/vendors.min.css')}}">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-extended.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('css/components.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/forms/wizard.min.css')}}">
{{--    <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/extensions/toastr.min.css')}}">--}}
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

</head>
<!-- END: Head-->
<!-- BEGIN: Body-->
<body class="vertical-layout vertical-menu-modern 1-column   blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row mb-1">
        </div>
        <div class="content-body"><section class="flexbox-container">
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <div class="col-md-4 col-10 box-shadow-2 p-0">
                        <div class="card border-grey border-lighten-3 px-1 py-1 box-shadow-3 m-0">
                            <div class="card-body">
                    <span class="card-title text-center">
            			<img src="{{ asset('images/logo/logo1.png')}}" class="img-fluid mx-auto d-block pt-2" width="300" alt="logo">
            		</span>
                            </div>
                            <div class="card-body text-center">
                                <h3>Hi, {{$currentUser->name}}</h3>
                                <p>You're account is under <strong>verification</strong> process.
                                    <br> Please check back later or contact the administrator.</p>
                                <div class="mt-2"><i class="la la-cog spinner font-large-2"></i></div>

                                <a href="{{url('logout')}}" class="btn btn-info mt-2">LOGOUT</a>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<script src="{{asset('vendors/js/vendors.min.js')}}"></script>
<script src="{{asset('js/core/app-menu.min.js')}}"></script>
<script src="{{asset('js/core/app.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
</body>
<!-- END: Body-->
<!-- Mirrored from pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/html/ltr/vertical-modern-menu-template/register-simple.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Dec 2019 14:33:49 GMT -->
</html>
