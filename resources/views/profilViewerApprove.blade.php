@extends('template',['user'=>$currentUser])
@push('css_extend')
    <link rel="stylesheet" type="text/css" href="{{asset('css/pages/users.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/pickers/pickadate/pickadate.css')}}">
@endpush
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row mb-1">
                <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Doctor Profile</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Users</a>
                                </li>
                                <li class="breadcrumb-item active">{{$doctor->name}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body"><!-- Simple User Cards -->

                <!-- User Profile Cards with Stats -->
                <section id="user-profile-cards-with-stats" class="row mt-2">

                    <div class="col-xl-12 col-md-12 col-12">
                        <div class="card profile-card-with-stats">
                            <div class="text-center">
                                <div class="card-body">
                                    <img src="{{ asset('fotoProfil/'.$doctor->avatar_url)}}" class="rounded-circle mb-1" alt="Card image" style="width: 200px; height: 200px; object-fit: cover;>
                                </div>
                                <div class="card-body">
                                    <h2>{{$doctor->name}}</h2>
                                    <ul class="list-inline list-inline-pipe ">
                                        <li class="mb-1">{{$doctor->gelar}}</li>
                                    </ul>
                                    <h6 class="card-subtitle text-muted mb-2">{{$doctor->deskripsi}}</h6>
                                    <div class="text-left">
                                        <form class="form" id="addonForm" name="addonForm" enctype="multipart/form-data">
                                            @csrf
                                            <input type="text" id="user_id" class="form-control"  name="user_id" hidden value="{{$doctor->user_id}}">
                                            <input type="text" id="url_photo" class="form-control"  name="url_photo" hidden value="{{$doctor->avatar_url}}">
                                            <input type="text" id="url_dokumen" class="form-control"  name="url_dokumen" hidden value="{{$doctor->data_url}}">
                                            <input type="text" id="jam_buka_praktek_input" class="form-control"  name="jam_buka_praktek_input" hidden value="{{$doctor->jam_buka_praktek}}">
                                            <input type="text" id="jam_tutup_praktek_input" class="form-control"  name="jam_tutup_praktek_input" hidden value="{{$doctor->jam_tutup_praktek}}">

                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-user"></i> Informasi Personal</h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="nama">Nama Lengkap</label>
                                                            <input type="text" id="nama" class="form-control" placeholder="Nama Lengkap" name="nama" value="{{$doctor->name}}" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="email">Email</label>
                                                            <input type="email" id="email" class="form-control" placeholder="Email" name="email" value="{{$doctor->email}}" disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="no_hp">No Handphone</label>
                                                            <input type="number" id="no_hp" class="form-control" placeholder="No Handphone" name="no_hp" value="{{$doctor->no_hp}}" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="gelar">Gelar</label>
                                                            <select id="gelar" name="gelar" class="form-control" disabled>
                                                                <option value="0" selected="" disabled="">Gelar</option>
                                                                <option value="Dokter Umum">Dokter Umum</option>
                                                                <option value="Dokter Gigi">Dokter Gigi</option>
                                                                <option value="Dokter Spesialis">Dokter Spesialis</option>
                                                                <option value="Dokter Gigi Spesialis">Dokter Gigi Spesialis</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="deskripsi">Deskripsi Profil</label>
                                                            <textarea id="deskripsi" rows="3" class="form-control" name="deskripsi" placeholder="Deskripsi Profil" disabled>{{$doctor->deskripsi}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="tempat_praktik">Tempat Praktik</label>
                                                            <input type="text" id="tempat_praktik" class="form-control" placeholder="Tempat Praktik" name="tempat_praktik" value="{{$doctor->tempat_praktik}}" disabled>
                                                        </div>
                                                    </div>

                                                </div>

                                                {{--jadwal praktek--}}
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group mb-0">
                                                            <label>Jadwal Praktek</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <table id="doctorTable" class="table table-striped table-bordered zero-configuration">
                                                            <thead>
                                                            <tr>
                                                                <th style="width: 1px">Hari</th>
                                                                <th>Jam Buka</th>
                                                                <th>Jam Tutup</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td style="font-weight: bold">Senin</td>
                                                                <td>
                                                                    <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="senin_buka" name="senin_buka"/>
                                                                </td>
                                                                <td>
                                                                    <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="senin_tutup" name="senin_tutup"/>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="font-weight: bold">Selasa</td>
                                                                <td>
                                                                    <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="selasa_buka" name="selasa_buka"/>
                                                                </td>
                                                                <td>
                                                                    <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="selasa_tutup" name="selasa_tutup"/>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="font-weight: bold">Rabu</td>
                                                                <td>
                                                                    <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="rabu_buka" name="rabu_buka"/>
                                                                </td>
                                                                <td>
                                                                    <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="rabu_tutup" name="rabu_tutup"/>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="font-weight: bold">Kamis</td>
                                                                <td>
                                                                    <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="kamis_buka" name="kamis_buka"/>
                                                                </td>
                                                                <td>
                                                                    <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="kamis_tutup" name="kamis_tutup"/>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="font-weight: bold">Jumat</td>
                                                                <td>
                                                                    <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="jumat_buka" name="jumat_buka"/>
                                                                </td>
                                                                <td>
                                                                    <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="jumat_tutup" name="jumat_tutup"/>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="font-weight: bold">Sabtu</td>
                                                                <td>
                                                                    <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="sabtu_buka" name="sabtu_buka"/>
                                                                </td>
                                                                <td>
                                                                    <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="sabtu_tutup" name="sabtu_tutup"/>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="font-weight: bold">Minggu</td>
                                                                <td>
                                                                    <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="minggu_buka" name="minggu_buka"/>
                                                                </td>
                                                                <td>
                                                                    <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="minggu_tutup" name="minggu_tutup"/>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                                {{--keterangan praktek--}}
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="deskripsi">Keterangan Jadwal Praktek</label>
                                                            <textarea id="keterangan_praktek" rows="3" class="form-control" name="keterangan_praktek" placeholder="Keterangan Jadwal Praktek" maxlength="250">{{$jadwal->keterangan_praktek}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="no_sip">No SIP</label>
                                                            <input type="text" id="no_sip" class="form-control" placeholder="No SIP" name="no_sip" value="{{$doctor->no_sip}}" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="harga_konsultasi">Harga Konsultasi (Rp.)</label>
                                                            <div class="input-group mt-0">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">Rp.</span>
                                                                </div>
                                                                <input type="number" class="form-control" placeholder="Harga Konsultasi" id="harga_konsultasi" name="harga_konsultasi" value="{{$doctor->harga_konsultasi}}" disabled>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <h4 class="form-section"><i class="la la-paperclip"></i> Requirements</h4>

                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label>Dokumen Sertifikat (.pdf)</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group">

                                                                <a class="btn btn-success" href="{{ route('doctors.download', $doctor->id) }}">DOWNLOAD</a>
{{--                                                                <input type="file" name="dokumen_sertifikat" id="dokumen_sertifikat" accept="application/pdf" class="form-control-file">--}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-actions text-right">
                                                <button type="button" class="btn btn-info" id="approveBtn">
                                                    <i class="la la-check-square-o"></i> Approve
                                                </button>
                                            </div>
                                        </form>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- User Profile Cards with Stats -->
                <!-- User Profile Cards with Cover Image -->

            </div>
        </div>
    </div>
<!-- END: Content -->
@endsection
@push('ajax_crud')

    <script src="{{asset('vendors/js/pickers/pickadate/picker.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/picker.time.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/legacy.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/dateTime/moment-with-locales.min.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/daterange/daterangepicker.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('js/scripts/pickers/dateTime/pick-a-datetime.min.js')}}"></script>
<script>
    $(function() {
        $('#gelar').val("{{$doctor->gelar}}");
    });

    $('#approveBtn').click(function (e) {
        e.preventDefault();
        var user_id = {{$doctor->user_id}};
        var data = {
            user_id: user_id,
        }

        swal({
            title:"Mengirim Data ke Server",
            text:"Harap menunggu....",
            icon: "{{asset('images/logo/loading.gif')}}",
            buttons: false,
            closeOnClickOutside: false,
        });

        $.ajax({
            type: "GET",
            url: "{{ route('approveDoctor') }}",
            data: data,
            success: function (data) {
                swal.close();
                window.location.href = "{{ route('approvals.index') }}";
            },
            error: function (data) {
                swal.close();
                console.log('Error:', data);
            }
        });



    });

    $(document).ready(function() {
        defaultValueJadwal();
        $(".jadwal-praktek").css('pointer-events', 'none');
    });

    function defaultValueJadwal() {
        var seninBuka = $('#senin_buka').pickatime().pickatime('picker')
        var seninTutup = $('#senin_tutup').pickatime().pickatime('picker')

        var selasaBuka = $('#selasa_buka').pickatime().pickatime('picker')
        var selasaTutup = $('#selasa_tutup').pickatime().pickatime('picker')

        var rabuBuka = $('#rabu_buka').pickatime().pickatime('picker')
        var rabuTutup = $('#rabu_tutup').pickatime().pickatime('picker')

        var kamisBuka = $('#kamis_buka').pickatime().pickatime('picker')
        var kamisTutup = $('#kamis_tutup').pickatime().pickatime('picker')

        var jumatBuka = $('#jumat_buka').pickatime().pickatime('picker')
        var jumatTutup = $('#jumat_tutup').pickatime().pickatime('picker')

        var sabtuBuka = $('#sabtu_buka').pickatime().pickatime('picker')
        var sabtuTutup = $('#sabtu_tutup').pickatime().pickatime('picker')

        var mingguBuka = $('#minggu_buka').pickatime().pickatime('picker')
        var mingguTutup = $('#minggu_tutup').pickatime().pickatime('picker')

        const seninB = "{{$jadwal->senin_buka}}".split(":");
        seninBuka.set('select', [seninB[0],seninB[1]])
        const seninT = "{{$jadwal->senin_tutup}}".split(":");
        seninTutup.set('select', [seninT[0],seninT[1]]);

        const selasaB = "{{$jadwal->selasa_buka}}".split(":");
        selasaBuka.set('select', [selasaB[0],selasaB[1]]);
        const selasaT = "{{$jadwal->selasa_tutup}}".split(":");
        selasaTutup.set('select', [selasaT[0],selasaT[1]]);

        const rabuB = "{{$jadwal->rabu_buka}}".split(":");
        rabuBuka.set('select', [rabuB[0],rabuB[1]]);
        const rabuT = "{{$jadwal->rabu_tutup}}".split(":");
        rabuTutup.set('select', [rabuT[0],rabuT[1]]);

        const kamisB = "{{$jadwal->kamis_buka}}".split(":");
        kamisBuka.set('select', [kamisB[0],kamisB[1]]);
        const kamisT = "{{$jadwal->kamis_tutup}}".split(":");
        kamisTutup.set('select', [kamisT[0],kamisT[1]]);

        const jumatB = "{{$jadwal->jumat_buka}}".split(":");
        jumatBuka.set('select', [jumatB[0],jumatB[1]]);
        const jumatT = "{{$jadwal->jumat_tutup}}".split(":");
        jumatTutup.set('select', [jumatT[0],jumatT[1]]);

        const sabtuB = "{{$jadwal->sabtu_buka}}".split(":");
        sabtuBuka.set('select', [sabtuB[0],sabtuB[1]]);
        const sabtuT = "{{$jadwal->sabtu_tutup}}".split(":");
        sabtuTutup.set('select', [sabtuT[0],sabtuT[1]]);

        const mingguB = "{{$jadwal->minggu_buka}}".split(":");
        mingguBuka.set('select', [mingguB[0],mingguB[1]]);
        const mingguT = "{{$jadwal->minggu_tutup}}".split(":");
        mingguTutup.set('select', [mingguT[0],mingguT[1]]);
    }
</script>
@endpush
