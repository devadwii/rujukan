@extends('template')
@push('css_extend')
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/forms/selects/select2.min.css')}}">
@endpush
@section('content')
    <!-- BEGIN: Content -->
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row mb-1">
                <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">

                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="card">

                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="text-center">
                                <img id="logo-landing" src="{{asset('images/logo/logo1.png')}}" class="mb-2" alt="Card image">

                                <h1 class="font-large-3 text-bold-700 landing-text" style="color: #0B75B9">Welcome to MERUJUK.COM</h1>
                                <br>
                                <h2 class="landing-nama font-large-2">{{$currentUser->name}}</h2>
                                <h3 class="landing-gelar font-large-1">{{$currentUser->gelar}}</h3>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content -->
@endsection

@push('ajax_crud')
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#backBtn').click(function() {
                window.location.href = "rujukanKeluar";
            });

            $('#saveBtn').click(function(e) {
                e.preventDefault();
                $(this).html('Save');

                $.ajax({
                    data: $('#rujukanForm').serialize(),
                    url: "{{ route('rujukanKeluar.store') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (dataResult) {
                        window.location.href = "rujukanKeluar";
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Save Changes');
                    }
                });
            });
        });
    </script>
@endpush
