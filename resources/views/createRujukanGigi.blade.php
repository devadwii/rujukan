@extends('template')
@push('css_extend')
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/forms/selects/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/pickers/pickadate/pickadate.css')}}">
@endpush
@section('content')
    <!-- BEGIN: Content -->
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row mb-1">
                <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Surat Rujukan</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Buat Surat Rujukan Dokter Gigi Ke Dokter Spesialis
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="card">

                    <div class="card-content collapse show">
                        <div class="card-body">

                            <form class="form" id="rujukanForm" name="rujukanForm" enctype="multipart/form-data" >
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <input type="hidden" id="jenis_rujukan" name="jenis_rujukan" value="{{$currentUser->gelar}}">
                                    <input type="hidden" id="id_dokter_perujuk" name="id_dokter_perujuk" value="{{$currentUser->id}}">
                                    <input type="text" id="jam_merujuk_input" class="form-control"  name="jam_merujuk_input" hidden>

                                    <h4 class="form-section "><i class="la la-book"></i>Info Rujukan</h4>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <label for="joined_date">No Registrasi</label>
                                                <input type="text" id="no_registrasi" class="form-control" readonly name="no_registrasi" value="{{$nomorRegistrasi}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <label for="dokter_perujuk">Dokter Perujuk </label>
                                                <input type="text" id="dokter_perujuk" class="form-control" placeholder="Dokter Perujuk" name="dokter_perujuk" value="{{$currentUser->name}}" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <label for="id_dokter_rujukan">Dokter Rujukan</label>
                                                <select id="id_dokter_rujukan" name="id_dokter_rujukan" class="select2 form-control" onchange="updateJamRujukan()">
                                                    <option value="0" selected="" disabled="">Pilih Dokter Rujukan </option>
                                                    @foreach($docterRujukan as $dokter)
                                                        <option value="{{$dokter->id}}">{{$dokter->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="deskripsi">Keterangan Khusus Jadwal Praktek</label>
                                                <textarea id="keterangan_praktek" rows="3" class="form-control" name="keterangan_praktek" placeholder="Keterangan Jadwal Praktek" maxlength="250" readonly></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <label for="tanggal">Tanggal (mm/dd/yyyy)</label>
                                                <input type="date" id="tanggal" class="form-control" name="tanggal" value="<?php echo date('Y-m-d'); ?>" onchange="updateJamRujukan()"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Pilih Jam Merujuk</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <span class="ft-clock"></span>
                                                            </span>
                                                    </div>
                                                    <input type='text' class="form-control pickatime-custom" placeholder="Pilih Jam Merujuk" id="jam_merujuk" name="jam_merujuk" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <h4 class="form-section "><i class="la la-user"></i>Info Pasien</h4>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <label for="nama_pasien">Nama Pasien </label>
                                                <input type="text" id="nama_pasien" class="form-control" placeholder="Nama Pasien" name="nama_pasien">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <label for="jenis_kelamin">Jenis Kelamin</label>
                                                <select id="jenis_kelamin" name="jenis_kelamin" class="form-control">
                                                    <option value="0" selected="" disabled="">Jenis Kelamin</option>
                                                    <option value="1" >Laki-Laki</option>
                                                    <option value="2" >Perempuan</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <label for="umur">Umur</label>
                                                <input type="number" id="umur" class="form-control"  placeholder="Umur" name="umur" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <label for="no_telepon">No HP</label>
                                                <input type="number" id="no_telepon" class="form-control"  placeholder="No HP" name="no_telepon" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <label for="alamat">Alamat</label>
                                                <textarea id="alamat" name="alamat" placeholder="Alamat" class="form-control" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <h4 class="form-section "><i class="la la-info"></i>Info Diagnosa</h4>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <label for="keluhan">Keluhan</label>
                                                <textarea id="keluhan" name="keluhan" placeholder="Keluhan" class="form-control" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <label for="pemeriksaan_klinis">Pemeriksaan Klinis</label>
                                                <textarea id="pemeriksaan_klinis" name="pemeriksaan_klinis" placeholder="Pemeriksaan Klinis" class="form-control" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <label for="pemeriksaan_lab">Pemeriksaan Lab</label>
                                                <textarea id="pemeriksaan_lab" name="pemeriksaan_lab" placeholder="Pemeriksaan Lab" class="form-control" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <label for="diagnosa_sementara">Diagnosa Sementara</label>
                                                <textarea id="diagnosa_sementara" name="diagnosa_sementara" placeholder="Pemeriksaan Klinis" class="form-control" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <label for="terapi">Terapi / Obat yang telah diberikan</label>
                                                <textarea id="terapi" name="terapi" placeholder="Terapi / Obat yang telah diberikan" class="form-control" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="mt-2">Demikian surat ini kami kirim, mohon kesediaan untuk mengirim surat balasan. Pasien masih dalam perawatan kami.</p>
                                    <p>Atas perhatian dan kerjasamanya. Kami ucapkan Terimakasih.</p>
                                </div>

                                <div class="form-actions text-right">
                                    <button id='backBtn' type="button" class="btn btn-warning mr-1">
                                        <i class="ft-x"></i> Batal
                                    </button>
                                    <button id="saveBtn"  value="create"  type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> Buat Surat Rujukan
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content -->
@endsection

@push('ajax_crud')
    <script src="{{asset('vendors/js/pickers/pickadate/picker.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/picker.time.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/legacy.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/dateTime/moment-with-locales.min.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/daterange/daterangepicker.js')}}"></script>
    <script type="text/javascript">
        $(".pickatime-custom").pickatime({
            formatSubmit: 'HH:i',
            hiddenName: true,
            editable: false
        })

        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#backBtn').click(function() {
                window.location.href = "rujukanKeluar";
            });

            $('#saveBtn').click(function(e) {
                e.preventDefault();
                $(this).html('Save');

                var merujuk = $('#jam_merujuk').pickatime().pickatime('picker')
                $('#jam_merujuk_input').val(merujuk.get('select','HH:i'));

                swal({
                    title:"Mengirim Surat Rujukan",
                    text:"Harap menunggu....",
                    icon: "{{asset('images/logo/loading.gif')}}",
                    buttons: false,
                    closeOnClickOutside: false,
                });

                $.ajax({
                    data: $('#rujukanForm').serialize(),
                    url: "{{ route('rujukanKeluar.store') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        if (data.errors) {
                            swal.close();
                            for(var errorMsg in data.errors) {
                                toastr.error(data.errors[errorMsg], "Gagal membuat surat rujukan !");
                            }
                        } else {
                            swal.close();
                            window.location.href = "rujukanKeluar";
                        }
                    },
                    error: function (data) {
                    }
                });

            });


        });

        function updateJamRujukan() {
            $('#jam_merujuk').val('');


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var dokterRujukan = document.getElementById("id_dokter_rujukan");
            var idDokter = dokterRujukan.value;

            var hariRujukan = document.getElementById("tanggal").value;

            if(idDokter === '0') {
                toastr.error("Silahkan pilih Dokter Rujukan terlebih dahulu !", "Peringatan !");
            } else {
                var data = {
                    idDokter: idDokter,
                    hariRujukan: hariRujukan
                }
                $.ajax({
                    type: "GET",
                    url: "ambilJamRujukan",
                    data: data,
                    success: function (data) {
                        var buka = data.jamRujukan.jam_buka_praktek
                        const bukaArray = buka.split(":");

                        var tutup = data.jamRujukan.jam_tutup_praktek
                        const tutupArray = tutup.split(":");

                        var jamMerujuk = $('#jam_merujuk').pickatime().pickatime('picker')
                        jamMerujuk.set('min',[bukaArray[0],bukaArray[1]])
                        jamMerujuk.set('max',[tutupArray[0],tutupArray[1]])

                        $('#keterangan_praktek').val(data.jadwal.keterangan_praktek);

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        }
    </script>
@endpush
