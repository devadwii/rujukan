@extends('template')
@section('content')
    <!-- BEGIN: Content -->
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row mb-1">
                <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">List Balasan</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item active">List Balasan
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header pb-0">
                                    <h4 class="card-title">List Balasan</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <table id="balasanTable" class="table table-striped table-bordered zero-configuration" width="100%">
                                                <thead>
                                                <tr>
                                                    <th width="30px">No</th>
                                                    <th>Tanggal</th>
                                                    <th>Nama Pasien</th>
                                                    @if ($currentUser->gelar == 'Dokter Gigi' || $currentUser->gelar == 'Dokter Umum')
                                                        <th>Dokter Rujukan</th>
                                                    @endif
                                                    @if ($currentUser->gelar == 'Dokter Spesialis' || $currentUser->gelar == 'Dokter Gigi Spesialis' ||$currentUser->gelar == 'Admin')
                                                        <th>Dokter Perujuk</th>
                                                    @endif
                                                    <th width="250px">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th width="30px">No</th>
                                                    <th>Tanggal</th>
                                                    <th>Nama Pasien</th>
                                                    @if ($currentUser->gelar == 'Dokter Gigi' || $currentUser->gelar == 'Dokter Umum')
                                                        <th>Dokter Rujukan</th>
                                                    @endif
                                                    @if ($currentUser->gelar == 'Dokter Spesialis' || $currentUser->gelar == 'Dokter Gigi Spesialis' ||$currentUser->gelar == 'Admin')
                                                        <th>Dokter Perujuk</th>
                                                    @endif
                                                    <th width="250px">Action</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content -->
@endsection
@push('ajax_crud')
    <script type="text/javascript">
        $(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#balasanTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('balasan.index') }}",
                columns: [
                    {data: null},
                    {data: 'tanggal', name: 'tanggal'},
                    {data: 'nama_pasien', name: 'nama_pasien'},
                    {data: 'name', name: 'name'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            table.on('draw.dt', function () {
                var info = table.page.info();
                table.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1 + info.start;
                });
            });

            $('body').on('click', '.lihatBalasan', function () {
                var id_rujukan = $(this).data('id');
                window.location.href = "{{ route('viewBalasan') }}" +'?id_rujukan='+id_rujukan;
            });

        });
    </script>

@endpush
