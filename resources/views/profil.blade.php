@extends('template')
@push('css_extend')
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/pickers/pickadate/pickadate.css')}}">
@endpush
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row mb-1">
                <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">User Profile</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Users</a>
                                </li>
                                <li class="breadcrumb-item active">{{$currentUser->name}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body"><!-- Simple User Cards -->

                <!-- User Profile Cards with Stats -->
                <section id="user-profile-cards-with-stats" class="row mt-2">

                    <div class="col-xl-12 col-md-12 col-12">
                        <div class="card profile-card-with-stats">
                            <div class="text-center">
                                <div class="card-body">
                                    <img src="{{ asset('fotoProfil/'.$currentUser->avatar_url)}}" class="rounded-circle mb-1" alt="Card image" style="width: 200px; height: 200px; object-fit: cover;>
                                </div>
                                <div class="card-body">
                                    <h2>{{$currentUser->name}}</h2>
                                    <ul class="list-inline list-inline-pipe">
                                        <li>{{$currentUser->gelar}}</li>
                                    </ul>
                                    <h6 class="card-subtitle text-muted">{{$currentUser->deskripsi}}</h6>
                                    <div class="text-left">
                                        <form class="form" id="addonForm" name="addonForm" enctype="multipart/form-data">
                                            @csrf
                                            <input type="text" id="doctor_id" class="form-control"  name="doktor_id" hidden value="{{$doctor->doctor_id}}">
                                            <input type="text" id="jam_buka_praktek_input" class="form-control"  name="jam_buka_praktek_input" hidden value="{{$currentUser->jam_buka_praktek}}">
                                            <input type="text" id="jam_tutup_praktek_input" class="form-control"  name="jam_tutup_praktek_input" hidden value="{{$currentUser->jam_tutup_praktek}}">

                                            <input type="text" id="senin_buka_input" class="form-control"  name="senin_buka_input" hidden>
                                            <input type="text" id="senin_tutup_input" class="form-control"  name="senin_tutup_input" hidden>

                                            <input type="text" id="selasa_buka_input" class="form-control"  name="selasa_buka_input" hidden value="">
                                            <input type="text" id="selasa_tutup_input" class="form-control"  name="selasa_tutup_input" hidden value="">

                                            <input type="text" id="rabu_buka_input" class="form-control"  name="rabu_buka_input" hidden value="">
                                            <input type="text" id="rabu_tutup_input" class="form-control"  name="rabu_tutup_input" hidden value="">

                                            <input type="text" id="kamis_buka_input" class="form-control"  name="kamis_buka_input" hidden value="">
                                            <input type="text" id="kamis_tutup_input" class="form-control"  name="kamis_tutup_input" hidden value="">

                                            <input type="text" id="jumat_buka_input" class="form-control"  name="jumat_buka_input" hidden value="">
                                            <input type="text" id="jumat_tutup_input" class="form-control"  name="jumat_tutup_input" hidden value="">

                                            <input type="text" id="sabtu_buka_input" class="form-control"  name="sabtu_buka_input" hidden value="">
                                            <input type="text" id="sabtu_tutup_input" class="form-control"  name="sabtu_tutup_input" hidden value="">

                                            <input type="text" id="minggu_buka_input" class="form-control"  name="minggu_buka_input" hidden value="">
                                            <input type="text" id="minggu_tutup_input" class="form-control"  name="minggu_tutup_input" hidden value="">

                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-user"></i> Informasi Personal</h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="nama">Nama Lengkap</label>
                                                            <input type="text" id="nama" class="form-control" placeholder="Nama Lengkap" name="nama" value="{{$currentUser->name}}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="email">Email</label>
                                                            <input type="email" id="email" class="form-control" placeholder="Email" name="email" value="{{$currentUser->email}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="no_hp">No Handphone</label>
                                                            <input type="number" id="no_hp" class="form-control" placeholder="No Handphone" name="no_hp" value="{{$currentUser->no_hp}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="gelar">Gelar</label>
                                                            <select id="gelar" name="gelar" class="form-control">
                                                                <option value="0" selected="" disabled="">Gelar</option>
                                                                <option value="Dokter Umum">Dokter Umum</option>
                                                                <option value="Dokter Gigi">Dokter Gigi</option>
                                                                <option value="Dokter Spesialis">Dokter Spesialis</option>
                                                                <option value="Dokter Gigi Spesialis">Dokter Gigi Spesialis</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="deskripsi">Deskripsi Profil</label>
                                                            <textarea id="deskripsi" rows="3" class="form-control" name="deskripsi" placeholder="Deskripsi Profil" maxlength="250">{{$currentUser->deskripsi}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="tempat_praktik">Tempat Praktik</label>
                                                            <input type="text" id="tempat_praktik" class="form-control" placeholder="Tempat Praktik" name="tempat_praktik" value="{{$currentUser->tempat_praktik}}">
                                                        </div>
                                                    </div>

                                                </div>

                                                {{--jadwal praktek--}}
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group mb-0">
                                                            <label>Jadwal Praktek</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <table id="doctorTable" class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                <tr>
                                                    <th style="width: 1px">Hari</th>
                                                    <th>Jam Buka</th>
                                                    <th>Jam Tutup</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td style="font-weight: bold">Senin</td>
                                                    <td>
                                                        <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="senin_buka" name="senin_buka"/>
                                                    </td>
                                                    <td>
                                                        <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="senin_tutup" name="senin_tutup"/>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="font-weight: bold">Selasa</td>
                                                    <td>
                                                        <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="selasa_buka" name="selasa_buka"/>
                                                    </td>
                                                    <td>
                                                        <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="selasa_tutup" name="selasa_tutup"/>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="font-weight: bold">Rabu</td>
                                                    <td>
                                                        <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="rabu_buka" name="rabu_buka"/>
                                                    </td>
                                                    <td>
                                                        <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="rabu_tutup" name="rabu_tutup"/>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="font-weight: bold">Kamis</td>
                                                    <td>
                                                        <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="kamis_buka" name="kamis_buka"/>
                                                    </td>
                                                    <td>
                                                        <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="kamis_tutup" name="kamis_tutup"/>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="font-weight: bold">Jumat</td>
                                                    <td>
                                                        <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="jumat_buka" name="jumat_buka"/>
                                                    </td>
                                                    <td>
                                                        <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="jumat_tutup" name="jumat_tutup"/>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="font-weight: bold">Sabtu</td>
                                                    <td>
                                                        <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="sabtu_buka" name="sabtu_buka"/>
                                                    </td>
                                                    <td>
                                                        <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="sabtu_tutup" name="sabtu_tutup"/>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="font-weight: bold">Minggu</td>
                                                    <td>
                                                        <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="minggu_buka" name="minggu_buka"/>
                                                    </td>
                                                    <td>
                                                        <input type='text' class="form-control pickatime-custom jadwal-praktek" placeholder="" id="minggu_tutup" name="minggu_tutup"/>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                                    </div>
                                                </div>

                                                {{--keterangan praktek--}}
                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="deskripsi">Keterangan Jadwal Praktek</label>
                                                            <textarea id="keterangan_praktek" rows="3" class="form-control" name="keterangan_praktek" placeholder="Keterangan Jadwal Praktek" maxlength="250">{{$jadwal->keterangan_praktek}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>

{{--                                                <div class="row">--}}
{{--                                                    <div class="col-md-6">--}}
{{--                                                        <div class="form-group">--}}
{{--                                                            <label>Jam Buka Praktek</label>--}}
{{--                                                            <div class="input-group">--}}
{{--                                                                <div class="input-group-prepend">--}}
{{--                                                                    <span class="input-group-text">--}}
{{--                                                                        <span class="ft-clock"></span>--}}
{{--                                                                    </span>--}}
{{--                                                                </div>--}}
{{--                                                                <input type='text' class="form-control pickatime-custom" placeholder="Set Jam Buka Praktek" id="jam_buka_praktek" name="jam_buka_praktek"/>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="col-md-6">--}}
{{--                                                        <div class="form-group">--}}
{{--                                                            <label>Jam Tutup Praktek</label>--}}
{{--                                                            <div class="input-group">--}}
{{--                                                                <div class="input-group-prepend">--}}
{{--                                                                    <span class="input-group-text">--}}
{{--                                                                        <span class="ft-clock"></span>--}}
{{--                                                                    </span>--}}
{{--                                                                </div>--}}
{{--                                                                <input type='text' class="form-control pickatime-custom" placeholder="Set Jam Tutup Praktek" id="jam_tutup_praktek" name="jam_tutup_praktek"/>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="no_sip">No SIP</label>
                                                            <input type="text" id="no_sip" class="form-control" placeholder="No SIP" name="no_sip" value="{{$currentUser->no_sip}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="harga_konsultasi">Harga Konsultasi (Rp.)</label>
                                                            <div class="input-group mt-0">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">Rp.</span>
                                                                </div>
                                                                <input type="number" class="form-control" placeholder="Harga Konsultasi" id="harga_konsultasi" name="harga_konsultasi" value="{{$currentUser->harga_konsultasi}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <h4 class="form-section"><i class="la la-paperclip"></i> Requirements</h4>

                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label>Dokumen Sertifikat (.pdf)</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <a class="btn btn-success" href="{{ route('doctors.download', $currentUser->id) }}">DOWNLOAD</a>
{{--                                                                <input type="file" name="dokumen_sertifikat" id="dokumen_sertifikat" accept="application/pdf" class="form-control-file">--}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-actions text-right">
                                                <button id='backBtn' type="button" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Kembali
                                                </button>
                                                <button type="button" class="btn btn-info" id="saveBtn">
                                                    <i class="la la-check-square-o"></i> Simpan
                                                </button>
                                            </div>

                                        </form>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- User Profile Cards with Stats -->
                <!-- User Profile Cards with Cover Image -->

            </div>
        </div>
    </div>
<!-- END: Content -->
@endsection
@push('ajax_crud')

    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('vendors/js/pickers/pickadate/picker.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/picker.time.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/legacy.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/dateTime/moment-with-locales.min.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/daterange/daterangepicker.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('js/scripts/pickers/dateTime/pick-a-datetime.min.js')}}"></script>
    <!-- END: Page JS-->
    <script>
        $(".pickatime-custom").pickatime({
            formatSubmit: 'HH:i',
            hiddenName: true
        })


        $(document).ready(function() {
            defaultValueJadwal();

            var buka = $('#jam_buka_praktek').pickatime().pickatime('picker')
            var inputBuka = document.getElementById("jam_buka_praktek_input").value;
            const bukaArray = inputBuka.split(":");
            buka.set('select', [bukaArray[0],bukaArray[1]])

            var tutup = $('#jam_tutup_praktek').pickatime().pickatime('picker')
            var inputTutup = document.getElementById("jam_tutup_praktek_input").value;
            const tutupArray = inputTutup.split(":");
            tutup.set('select', [tutupArray[0],tutupArray[1]])

            $('#gelar').val("{{$currentUser->gelar}}");
        });

        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#backBtn').click(function() {
                window.location.href = "home";
            });


            $('#saveBtn').click(function (e) {
                var buka = $('#jam_buka_praktek').pickatime().pickatime('picker')
                var tutup = $('#jam_tutup_praktek').pickatime().pickatime('picker')

                $('#jam_buka_praktek_input').val(buka.get('select','HH:i'));
                $('#jam_tutup_praktek_input').val(tutup.get('select','HH:i'));

                e.preventDefault();

                setInputForJadwal();

                swal({
                    title:"Menyimpan data",
                    text:"Harap menunggu....",
                    icon: "{{asset('images/logo/loading.gif')}}",
                    buttons: false,
                    closeOnClickOutside: false,
                });

                $.ajax({
                    data: new FormData($("#addonForm")[0]),
                    url: "{{ route('updateProfil') }}",
                    type: "POST",
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function (data) {

                        if (data.errors) {
                            swal.close();
                            for(var errorMsg in data.errors) {
                                toastr.error(data.errors[errorMsg], "Gagal update profil !");
                            }
                        } else {
                            swal.close();
                            swal("Berhasil!", "Data profil berhasil di Update!", "success");
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });

        });

        function defaultValueJadwal() {
            var seninBuka = $('#senin_buka').pickatime().pickatime('picker')
            var seninTutup = $('#senin_tutup').pickatime().pickatime('picker')

            var selasaBuka = $('#selasa_buka').pickatime().pickatime('picker')
            var selasaTutup = $('#selasa_tutup').pickatime().pickatime('picker')

            var rabuBuka = $('#rabu_buka').pickatime().pickatime('picker')
            var rabuTutup = $('#rabu_tutup').pickatime().pickatime('picker')

            var kamisBuka = $('#kamis_buka').pickatime().pickatime('picker')
            var kamisTutup = $('#kamis_tutup').pickatime().pickatime('picker')

            var jumatBuka = $('#jumat_buka').pickatime().pickatime('picker')
            var jumatTutup = $('#jumat_tutup').pickatime().pickatime('picker')

            var sabtuBuka = $('#sabtu_buka').pickatime().pickatime('picker')
            var sabtuTutup = $('#sabtu_tutup').pickatime().pickatime('picker')

            var mingguBuka = $('#minggu_buka').pickatime().pickatime('picker')
            var mingguTutup = $('#minggu_tutup').pickatime().pickatime('picker')

            const seninB = "{{$jadwal->senin_buka}}".split(":");
            seninBuka.set('select', [seninB[0],seninB[1]])
            const seninT = "{{$jadwal->senin_tutup}}".split(":");
            seninTutup.set('select', [seninT[0],seninT[1]]);

            const selasaB = "{{$jadwal->selasa_buka}}".split(":");
            selasaBuka.set('select', [selasaB[0],selasaB[1]]);
            const selasaT = "{{$jadwal->selasa_tutup}}".split(":");
            selasaTutup.set('select', [selasaT[0],selasaT[1]]);

            const rabuB = "{{$jadwal->rabu_buka}}".split(":");
            rabuBuka.set('select', [rabuB[0],rabuB[1]]);
            const rabuT = "{{$jadwal->rabu_tutup}}".split(":");
            rabuTutup.set('select', [rabuT[0],rabuT[1]]);

            const kamisB = "{{$jadwal->kamis_buka}}".split(":");
            kamisBuka.set('select', [kamisB[0],kamisB[1]]);
            const kamisT = "{{$jadwal->kamis_tutup}}".split(":");
            kamisTutup.set('select', [kamisT[0],kamisT[1]]);

            const jumatB = "{{$jadwal->jumat_buka}}".split(":");
            jumatBuka.set('select', [jumatB[0],jumatB[1]]);
            const jumatT = "{{$jadwal->jumat_tutup}}".split(":");
            jumatTutup.set('select', [jumatT[0],jumatT[1]]);

            const sabtuB = "{{$jadwal->sabtu_buka}}".split(":");
            sabtuBuka.set('select', [sabtuB[0],sabtuB[1]]);
            const sabtuT = "{{$jadwal->sabtu_tutup}}".split(":");
            sabtuTutup.set('select', [sabtuT[0],sabtuT[1]]);

            const mingguB = "{{$jadwal->minggu_buka}}".split(":");
            mingguBuka.set('select', [mingguB[0],mingguB[1]]);
            const mingguT = "{{$jadwal->minggu_tutup}}".split(":");
            mingguTutup.set('select', [mingguT[0],mingguT[1]]);
        }

        function setInputForJadwal() {

            var seninBuka = $('#senin_buka').pickatime().pickatime('picker');
            var seninTutup = $('#senin_tutup').pickatime().pickatime('picker');

            var selasaBuka = $('#selasa_buka').pickatime().pickatime('picker');
            var selasaTutup = $('#selasa_tutup').pickatime().pickatime('picker');

            var rabuBuka = $('#rabu_buka').pickatime().pickatime('picker');
            var rabuTutup = $('#rabu_tutup').pickatime().pickatime('picker');

            var kamisBuka = $('#kamis_buka').pickatime().pickatime('picker');
            var kamisTutup = $('#kamis_tutup').pickatime().pickatime('picker');

            var jumatBuka = $('#jumat_buka').pickatime().pickatime('picker');
            var jumatTutup = $('#jumat_tutup').pickatime().pickatime('picker');

            var sabtuBuka = $('#sabtu_buka').pickatime().pickatime('picker');
            var sabtuTutup = $('#sabtu_tutup').pickatime().pickatime('picker');

            var mingguBuka = $('#minggu_buka').pickatime().pickatime('picker');
            var mingguTutup = $('#minggu_tutup').pickatime().pickatime('picker');

            $('#senin_buka_input').val(seninBuka.get('select','HH:i'));
            $('#senin_tutup_input').val(seninTutup.get('select','HH:i'));

            $('#selasa_buka_input').val(selasaBuka.get('select','HH:i'));
            $('#selasa_tutup_input').val(selasaTutup.get('select','HH:i'));

            $('#rabu_buka_input').val(rabuBuka.get('select','HH:i'));
            $('#rabu_tutup_input').val(rabuTutup.get('select','HH:i'));

            $('#kamis_buka_input').val(kamisBuka.get('select','HH:i'));
            $('#kamis_tutup_input').val(kamisTutup.get('select','HH:i'));

            $('#jumat_buka_input').val(jumatBuka.get('select','HH:i'));
            $('#jumat_tutup_input').val(jumatTutup.get('select','HH:i'));

            $('#sabtu_buka_input').val(sabtuBuka.get('select','HH:i'));
            $('#sabtu_tutup_input').val(sabtuTutup.get('select','HH:i'));

            $('#minggu_buka_input').val(mingguBuka.get('select','HH:i'));
            $('#minggu_tutup_input').val(mingguTutup.get('select','HH:i'));

        }

    </script>

@endpush
