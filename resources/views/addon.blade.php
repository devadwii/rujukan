<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>Register Page</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/vendors.min.css')}}">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-extended.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/pickers/pickadate/pickadate.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('css/components.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/forms/wizard.min.css')}}">
    {{--    <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/extensions/toastr.min.css')}}">--}}
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/sweetalert.css')}}">

</head>
<!-- END: Head-->
<!-- BEGIN: Body-->
<body class="vertical-layout vertical-menu-modern 1-column fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
{{--<body class="vertical-layout vertical-menu-modern 1-column   blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">--}}
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row mb-1">
        </div>
        <div class="content-body">
            <section class="flexbox-container">
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                        <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
                            <div class="card-header border-0">
                                <div class="card-title text-center">
                                    <img src="{{asset('images/logo/logo1.png')}}" alt="branding logo" width="150">
                                </div>

                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form class="form" id="addonForm" name="addonForm" enctype="multipart/form-data">
                                        @csrf
                                        <input type="text" id="url_photo" class="form-control"  name="url_photo" hidden>
                                        <input type="text" id="url_dokumen" class="form-control"  name="url_dokumen" hidden>
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="ft-user"></i> Personal Info</h4>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="no_hp">No Handphone</label>
                                                        <input type="number" id="no_hp" class="form-control" placeholder="No Handphone" name="no_hp">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="gelar">Gelar</label>
                                                        <select id="gelar" name="gelar" class="form-control">
                                                            <option value="0" selected="" disabled="">Gelar</option>
                                                            <option value="Dokter Umum">Dokter Umum</option>
                                                            <option value="Dokter Gigi">Dokter Gigi</option>
                                                            <option value="Dokter Spesialis">Dokter Spesialis</option>
                                                            <option value="Dokter Gigi Spesialis">Dokter Gigi Spesialis</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="deskripsi">Deskripsi Profil</label>
                                                        <textarea id="deskripsi" rows="3" class="form-control" name="deskripsi" placeholder="Deskripsi Profil" maxlength="250"></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="tempat_praktik">Tempat Praktik</label>
                                                        <input type="text" id="tempat_praktik" class="form-control" placeholder="Tempat Praktik" name="tempat_praktik">
                                                    </div>
                                                </div>

                                            </div>

{{--                                            <div class="row">--}}
{{--                                                <div class="col-md-6">--}}
{{--                                                    <div class="form-group">--}}
{{--                                                        <label>Jam Buka Praktek</label>--}}
{{--                                                        <div class="input-group">--}}
{{--                                                            <div class="input-group-prepend">--}}
{{--                                                                    <span class="input-group-text">--}}
{{--                                                                        <span class="ft-clock"></span>--}}
{{--                                                                    </span>--}}
{{--                                                            </div>--}}
{{--                                                            <input type='text' class="form-control pickatime-custom" placeholder="Set Jam Buka Praktek" id="jam_buka_praktek" name="jam_buka_praktek"/>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="col-md-6">--}}
{{--                                                    <div class="form-group">--}}
{{--                                                        <label>Jam Tutup Praktek</label>--}}
{{--                                                        <div class="input-group">--}}
{{--                                                            <div class="input-group-prepend">--}}
{{--                                                                    <span class="input-group-text">--}}
{{--                                                                        <span class="ft-clock"></span>--}}
{{--                                                                    </span>--}}
{{--                                                            </div>--}}
{{--                                                            <input type='text' class="form-control pickatime-custom" placeholder="Set Jam Tutup Praktek" id="jam_tutup_praktek" name="jam_tutup_praktek"/>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="no_sip">No SIP</label>
                                                        <input type="text" id="no_sip" class="form-control" placeholder="No SIP" name="no_sip">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="harga_konsultasi">Harga Konsultasi (Rp.)</label>
                                                        <div class="input-group mt-0">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">Rp.</span>
                                                            </div>
                                                            <input type="number" class="form-control" placeholder="Harga Konsultasi" id="harga_konsultasi" name="harga_konsultasi">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <h4 class="form-section"><i class="la la-paperclip"></i> Requirements</h4>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Upload Foto Profil</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="file" name="foto_profil" id="foto_profil" accept="image/*" class="form-control-file">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Upload Dokumen Sertifikat (.pdf)</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="file" name="dokumen_sertifikat" id="dokumen_sertifikat" accept="application/pdf" class="form-control-file">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-actions text-right">
                                            <button type="button" class="btn btn-info" id="saveBtn">
                                                <i class="la la-check-square-o"></i> Simpan
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<script src="{{asset('vendors/js/vendors.min.js')}}"></script>
<script src="{{asset('js/core/app-menu.min.js')}}"></script>
<script src="{{asset('js/core/app.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="{{asset('vendors/js/pickers/pickadate/picker.js')}}"></script>
<script src="{{asset('vendors/js/pickers/pickadate/picker.date.js')}}"></script>
<script src="{{asset('vendors/js/pickers/pickadate/picker.time.js')}}"></script>
<script src="{{asset('vendors/js/pickers/pickadate/legacy.js')}}"></script>
<script src="{{asset('vendors/js/pickers/dateTime/moment-with-locales.min.js')}}"></script>
<script src="{{asset('vendors/js/pickers/daterange/daterangepicker.js')}}"></script>
<!-- END: Page Vendor JS-->
<script src="{{asset('vendors/js/extensions/sweetalert.min.js')}}"></script>
<script src="{{asset('js/scripts/extensions/sweet-alerts.min.js')}}"></script>

<!-- BEGIN: Page JS-->
<script src="{{asset('js/scripts/pickers/dateTime/pick-a-datetime.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(e) {

        $('#foto_profil').change(function() {
            // let reader = new FileReader();
            // reader.onload = (e) => {
            //     $('#preview-image1-before-upload').attr('src', e.target.result);
            //     $('#imageModal1').attr('src', e.target.result);
            // }
            // reader.readAsDataURL(this.files[0]);
            var filename = $('#foto_profil')[0].files[0];
            $('#url_photo').val(filename);

        });

        $('#dokumen_sertifikat').change(function() {
            // let reader = new FileReader();
            // reader.onload = (e) => {
            //     $('#preview-image1-before-upload').attr('src', e.target.result);
            //     $('#imageModal1').attr('src', e.target.result);
            // }
            // reader.readAsDataURL(this.files[0]);
            var filenameDokumen = $('#dokumen_sertifikat')[0].files[0];
            $('#url_dokumen').val(filenameDokumen);

        });

    });
</script>

<script>
    $(".pickatime-custom").pickatime({
        formatSubmit: 'HH:i',
        hiddenName: true
    })
</script>
<script type="text/javascript">
    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#saveBtn').click(function (e) {
            // var buka = $('#jam_buka_praktek').pickatime().pickatime('picker')
            // var tutup = $('#jam_tutup_praktek').pickatime().pickatime('picker')
            //
            // $('#jam_buka_praktek_input').val(buka.get('select','HH:i'));
            // $('#jam_tutup_praktek_input').val(tutup.get('select','HH:i'));

            e.preventDefault();

            swal({
                title:"Mengirim Data ke Server",
                text:"Harap menunggu....",
                icon: "{{asset('images/logo/loading.gif')}}",
                buttons: false,
                closeOnClickOutside: false,
            });

            $.ajax({
                data: new FormData($("#addonForm")[0]),
                url: "{{ route('doctors.store') }}",
                type: "POST",
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data.errors) {
                        swal.close();
                        for(var errorMsg in data.errors) {
                            toastr.error(data.errors[errorMsg], "Failed to submit data !");
                        }
                    } else {
                        swal.close();
                        window.location.href = "home";
                    }
                },
                error: function (data) {

                    console.log('Error:', data);
                    $('#saveBtn').html('Save Changes');
                }
            });
        });

    });
</script>


</body>
<!-- END: Body-->
</html>
