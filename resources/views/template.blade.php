<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
<!-- Mirrored from pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/html/ltr/vertical-modern-menu-template/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Dec 2019 14:30:44 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>MERUJUK</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset('images/logo/logo-kecil.png')}}" >
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/vendors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/charts/jqvmap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-extended.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/colors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/components.min.css')}}">
    <!-- END: Theme CSS-->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/core/menu/menu-types/vertical-menu-modern.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/core/colors/palette-gradient.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/charts/jquery-jvectormap-2.0.3.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/charts/morris.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/simple-line-icons/style.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/core/colors/palette-gradient.min.css')}}">


    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/sweetalert.css')}}">

@stack('css_extend')

    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
<!-- END: Page CSS-->
</head>
<!-- END: Head-->
<!-- BEGIN: Body-->
<body class="vertical-layout vertical-menu-modern 2-columns fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-lg-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item mr-auto">
                    <a class="navbar-brand" href="{{url('home')}}">
                        <img class="brand-logo " src="{{asset('images/logo/logo-kecil.png')}}">
                        <h3 class="brand-text">MERUJUK</h3></a>
                </li>

                <li class="nav-item d-lg-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a></li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
                </ul>
                <ul class="nav navbar-nav float-right">
                </ul>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"><span id="user-identity" class="mr-1 user-name text-bold-700">{{$currentUser->name}} / {{$currentUser->gelar}}</span><span class="avatar avatar-online"><img src="{{ asset('fotoProfil/'.$currentUser->avatar_url)}}" alt="avatar"><i></i></span></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            @if ($currentUser->gelar != 'Admin')
                                <a class="dropdown-item" href="{{url('editProfile')}}"><i class="ft-user"></i>Profile</a>
                                <div class="dropdown-divider"></div>
                            @endif
                            <a class="dropdown-item" href="{{url('logout')}}"><i class="ft-power"></i> Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" navigation-header"><span>Main Menu</span><i class="la la-ellipsis-h" data-toggle="tooltip" data-placement="right" data-original-title="Main Menu"></i></li>
            @if ($currentUser->gelar == 'Dokter Umum' || $currentUser->gelar == 'Dokter Gigi')
            <li class="@yield('rujukanKeluar') nav-item"><a href="{{url('rujukanKeluar')}}"><i class="la la-share-square"></i><span class="menu-title" >Rujukan Keluar</span></a></li>
            <li class="@yield('rujukanBalasan') nav-item"><a href="{{url('balasan')}}"><i class="la la-reply-all"></i><span class="menu-title" >Rujukan Balasan</span></a></li>
            @endif

            @if ($currentUser->gelar == 'Dokter Spesialis' || $currentUser->gelar == 'Dokter Gigi Spesialis')
            <li class="@yield('rujukanMasuk') nav-item"><a href="{{url('rujukanMasuk')}}"><i class="la la-envelope-open"></i><span class="menu-title" >Rujukan Masuk</span></a></li>
            <li class="@yield('rujukanKeluar') nav-item"><a href="{{url('balasan')}}"><i class="la la-share-square"></i><span class="menu-title" >Balasan Keluar</span></a></li>
            @endif

            @if ($currentUser->gelar == 'Admin')
                <li class="@yield('rujukanMasuk') nav-item"><a href="{{url('rujukanMasuk')}}"><i class="la la-envelope-open"></i><span class="menu-title" >List Rujukan</span></a></li>
                <li class="@yield('rujukanKeluar') nav-item"><a href="{{url('balasan')}}"><i class="la la-reply-all"></i><span class="menu-title" >List Balasan</span></a></li>
            @endif

            <li class=" navigation-header"><span data-i18n="nav.category.admin-panels">Data</span><i class="la la-ellipsis-h" data-toggle="tooltip" data-placement="right" data-original-title="Admin Panels"></i>
            </li>
            <li class="@yield('listDokter') nav-item"><a href="{{url('doctors')}}"><i class="la la-users"></i><span class="menu-title" >List Dokter</span></a></li>

            @if ($currentUser->gelar == 'Admin')
                <li class=" navigation-header"><span data-i18n="nav.category.admin-panels">Admin Panel</span><i class="la la-ellipsis-h" data-toggle="tooltip" data-placement="right" data-original-title="Admin Panels"></i></li>
                <li class="@yield('menungguApproval') nav-item"><a href="{{url('approvals')}}"><i class="la la-history"></i>
                        <span class="menu-title" >List Approval</span>
                        <span id="counterApproval" class="badge badge badge-pill badge-warning float-right"></span>
                    </a></li>
            @endif


        </ul>
    </div>
</div>
<!-- END: Main Menu-->

<!-- BEGIN: Content -->
@yield('content')
<!-- END: Content -->

<!-- BEGIN: Footer-->
<footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright  &copy; 2021 </span><span class="float-md-right d-none d-lg-block">DevoID Project<span id="scroll-top"></span></span></p>
</footer>
<!-- END: Footer-->
<!-- BEGIN: Vendor JS-->
<script src="{{asset('vendors/js/vendors.min.js')}}"></script>

<!-- BEGIN Vendor JS-->
<!-- BEGIN: Page Vendor JS-->

<script src="{{asset('vendors/js/charts/chart.min.js')}}"></script>
<script src="{{asset('vendors/js/charts/raphael-min.js')}}"></script>
<script src="{{asset('vendors/js/charts/morris.min.js')}}"></script>
<script src="{{asset('vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js')}}"></script>
<script src="{{asset('vendors/js/charts/jvector/jquery-jvectormap-world-mill.js')}}"></script>
<script src="{{asset('data/jvector/visitor-data.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- chart pie -->

<!-- chart line -->

<!-- datatable -->
<script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>

<!-- END: Page Vendor JS-->
<!-- BEGIN: Theme JS-->
<script src="{{asset('js/core/app-menu.min.js')}}"></script>
<script src="{{asset('js/core/app.min.js')}}"></script>
<script src="{{asset('js/scripts/customizer.min.js')}}"></script>
<script src="{{asset('js/scripts/footer.min.js')}}"></script>


<script src="{{asset('vendors/js/extensions/sweetalert.min.js')}}"></script>
<script src="{{asset('js/scripts/extensions/sweet-alerts.min.js')}}"></script>
<!-- END: Theme JS-->
<!-- BEGIN: Page JS-->

<script type="text/javascript">
    $(function () {
        $('body').on('click', '.viewProfile', function () {
            var user_id = $(this).data('id');
            window.location.href = "{{ route('doctors.index') }}" +'/' + user_id +'/edit';
        });

        if ('{{$currentUser->gelar}}' == "Admin") {
            $.ajax({
                url: "{{ route('getNumberApproval') }}",
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    $('#counterApproval').text(data.data);
                },
                error: function (data) {

                }
            });
        }

    });
</script>



@stack('ajax_crud')
<!-- BEGIN Vendor JS-->
<!-- BEGIN: Page Vendor JS-->


</body>
<!-- END: Body-->
</html>
