@extends('template')
@push('css_extend')
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/forms/selects/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/pickers/pickadate/pickadate.css')}}">
@endpush
@section('content')
    <!-- BEGIN: Content -->
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row mb-1">
                <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Surat Balasan</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Buat Surat Balasan
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="card">

                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form class="form" id="balasanForm" name="balasanForm" enctype="multipart/form-data" >
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <input type="hidden" id="id_rujukan" name="id_rujukan" value="{{$rujukan->id}}">
                                    <input type="text" id="jam_rujuk_input" class="form-control"  name="jam_rujuk_input" hidden value="{{$rujukan->jam_rujuk}}">
                                    <div class="nav-vertical">
                                        <ul class="nav nav-tabs nav-left nav-border-left">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab"  href="#tabRujukan" aria-expanded="true">Rujukan</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab"  href="#tabBalasan" aria-expanded="false">Balasan</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content px-1">
                                            <div class="tab-pane active" id="tabRujukan">
                                                <div class="col-12">
                                                    <h4 class="form-section "><i class="la la-book"></i>Info Rujukan</h4>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="joined_date">No Registrasi</label>
                                                                <input type="text" id="no_registrasi" class="form-control" readonly name="no_registrasi" value="{{$rujukan->no_registrasi}}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="dokter_perujuk">Dokter Perujuk </label>
                                                                <input type="text" id="dokter_perujuk" class="form-control" placeholder="Dokter Perujuk" name="dokter_perujuk" value="{{$dokterPerujuk->name}}" readonly>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="id_dokter_rujukan">Dokter Rujukan</label>
                                                                <select id="id_dokter_rujukan" name="id_dokter_rujukan" class="select2 form-control" disabled>
                                                                    <option value="0" selected="" disabled="">Pilih Dokter Rujukan </option>
                                                                    @foreach($docterRujukan as $dokter)
                                                                        <option value="{{$dokter->id}}">{{$dokter->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group ">
                                                                <label for="tanggal">Tanggal (mm/dd/yyyy)</label>
                                                                <input readonly type="date" id="tanggal" class="form-control" name="tanggal" value="{{$rujukan->tanggal}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Jam Merujuk</label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <span class="ft-clock"></span>
                                                            </span>
                                                                    </div>
                                                                    <input type='text' class="form-control pickatime-custom" placeholder="Jam Merujuk" id="jam_rujuk" name="jam_rujuk"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <h4 class="form-section "><i class="la la-user"></i>Info Pasien</h4>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="nama_pasien">Nama Pasien </label>
                                                                <input type="text" id="nama_pasien" class="form-control" placeholder="Nama Pasien" readonly name="nama_pasien" value="{{$rujukan->nama_pasien}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <label for="jenis_kelamin">Jenis Kelamin</label>
                                                                <select id="jenis_kelamin" name="jenis_kelamin" class="form-control" disabled>
                                                                    <option value="0" selected="" disabled="">Jenis Kelamin</option>
                                                                    <option value="1" >Laki-Laki</option>
                                                                    <option value="2" >Perempuan</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <label for="umur">Umur</label>
                                                                <input type="number" id="umur" class="form-control"  placeholder="Umur" readonly name="umur" value="{{$rujukan->umur}}" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <label for="no_telepon">No HP</label>
                                                                <input type="number" id="no_telepon" class="form-control" readonly placeholder="No HP" name="no_telepon" value="{{$rujukan->no_telepon}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="alamat">Alamat</label>
                                                                <textarea id="alamat" name="alamat" placeholder="Alamat" readonly class="form-control" rows="3" >{{$rujukan->alamat}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <h4 class="form-section "><i class="la la-info"></i>Info Diagnosa</h4>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="keluhan">Keluhan</label>
                                                                <textarea id="keluhan" name="keluhan" placeholder="Keluhan" readonly class="form-control" rows="3">{{$rujukan->keluhan}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="pemeriksaan_klinis">Pemeriksaan Klinis</label>
                                                                <textarea id="pemeriksaan_klinis" name="pemeriksaan_klinis" readonly placeholder="Pemeriksaan Klinis" class="form-control" rows="3">{{$rujukan->pemeriksaan_klinis}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="pemeriksaan_lab">Pemeriksaan Lab</label>
                                                                <textarea id="pemeriksaan_lab" name="pemeriksaan_lab" readonly placeholder="Pemeriksaan Lab" class="form-control" rows="3">{{$rujukan->pemeriksaan_lab}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="diagnosa_sementara">Diagnosa Sementara</label>
                                                                <textarea id="diagnosa_sementara" name="diagnosa_sementara" readonly placeholder="Pemeriksaan Klinis" class="form-control" rows="3">{{$rujukan->diagnosa_sementara}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="terapi">Terapi / Obat yang telah diberikan</label>
                                                                <textarea id="terapi" name="terapi" readonly placeholder="Terapi / Obat yang telah diberikan" class="form-control" rows="3" >{{$rujukan->terapi}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="tabBalasan" >
                                                <div class="col-12">
                                                    <h4 class="form-section "><i class="la la-book"></i>Info Balasan</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group ">
                                                                <label for="tanggal_balasan">Tanggal Balasan</label>
                                                                <input type="date" id="tanggal_balasan" class="form-control" name="tanggal_balasan">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group ">
                                                                <label for="no_registrasi_balasan">No Registrasi</label>
                                                                <input type="text" id="no_registrasi_balasan" class="form-control" readonly name="no_registrasi_balasan" value="{{$rujukan->no_registrasi}}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <h4 class="form-section "><i class="la la-user"></i>Info Pasien</h4>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="nama_pasien">Nama Pasien </label>
                                                                <input type="text" id="nama_pasien" class="form-control" placeholder="Nama Pasien" readonly name="nama_pasien" value="{{$rujukan->nama_pasien}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <label for="jenis_kelamin_balasan">Jenis Kelamin</label>
                                                                <select id="jenis_kelamin_balasan" name="jenis_kelamin_balasan" class="form-control" disabled>
                                                                    <option value="0" selected="" disabled="">Jenis Kelamin</option>
                                                                    <option value="1" >Laki-Laki</option>
                                                                    <option value="2" >Perempuan</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <label for="umur">Umur</label>
                                                                <input type="number" id="umur" class="form-control"  placeholder="Umur" readonly name="umur" value="{{$rujukan->umur}}" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <label for="no_telepon">No HP</label>
                                                                <input type="number" id="no_telepon" class="form-control" readonly placeholder="No HP" name="no_telepon" value="{{$rujukan->no_telepon}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="alamat">Alamat</label>
                                                                <textarea id="alamat" name="alamat" placeholder="Alamat" readonly class="form-control" rows="3" >{{$rujukan->alamat}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <h4 class="form-section "><i class="la la-info"></i>Info Keterangan</h4>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="konsul_selesai">Konsul Selesai</label>
                                                                <textarea id="konsul_selesai" name="konsul_selesai" placeholder="Konsul Selesai" class="form-control" rows="3"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="perlu_kontrol_kembali">Perlu Kontrol Kembali</label>
                                                                <textarea id="perlu_kontrol_kembali" name="perlu_kontrol_kembali" placeholder="Perlu Kontrol Kembali" class="form-control" rows="3"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="perlu_kontrol_ke_ahli">Perlu Kontrol Ke Ahli Lain</label>
                                                                <textarea id="perlu_kontrol_ke_ahli" name="perlu_kontrol_ke_ahli" placeholder="Perlu Kontrol Ke Ahli Lain" class="form-control" rows="3"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="perlu_dirawat">Perlu Dirawat Dengan Indikasi</label>
                                                                <textarea id="perlu_dirawat" name="perlu_dirawat" placeholder="Perlu Dirawat Dengan Indikasi" class="form-control" rows="3"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <h4 class="form-section "><i class="la la-info"></i>Hasil Pemeriksaan</h4>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="hasil_pemeriksaan">Hasil Pemeriksaan</label>
                                                                <textarea id="hasil_pemeriksaan" name="hasil_pemeriksaan" placeholder="Hasil Pemeriksaan" class="form-control" rows="3"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="diagnosa">Diagnosa</label>
                                                                <textarea id="diagnosa" name="diagnosa" placeholder="Diagnosa" class="form-control" rows="3"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <label for="perawatan_yang_sudah_dilakukan">Perawatan Yang Sudah Dilakukan</label>
                                                                <textarea id="perawatan_yang_sudah_dilakukan" name="perawatan_yang_sudah_dilakukan" placeholder="Perawatan Yang Sudah Dilakukan" class="form-control" rows="3"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p class="mt-2">Demikian balasan surat rujukan ini kami kirim. Atas perhatian dan kerjasamanya. Kami ucapkan Terimakasih.</p>
                                                </div>
                                                <div class="form-actions text-right">
                                                    <button id='backBtn' type="button" class="btn btn-warning mr-1">
                                                        <i class="ft-x"></i> Kembali
                                                    </button>
                                                    <button id="saveBtn"  value="create"  type="submit" class="btn btn-primary">
                                                        <i class="la la-check-square-o"></i> Kirim Surat Balasan
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content -->
@endsection

@push('ajax_crud')
    <script src="{{asset('vendors/js/pickers/pickadate/picker.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/picker.time.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/pickadate/legacy.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/dateTime/moment-with-locales.min.js')}}"></script>
    <script src="{{asset('vendors/js/pickers/daterange/daterangepicker.js')}}"></script>
    <script type="text/javascript">
        $(".pickatime-custom").pickatime({
            formatSubmit: 'HH:i',
            hiddenName: true
        })

        $(function() {
            $('#id_dokter_rujukan').val("{{$rujukan->id_dokter_rujukan}}");
            $('#jenis_kelamin').val("{{$rujukan->jenis_kelamin}}");
            $('#jenis_kelamin_balasan').val("{{$rujukan->jenis_kelamin}}");

            var jamRujuk = $('#jam_merujuk').pickatime().pickatime('picker')
            var inputJamRujuk = document.getElementById("jam_rujuk_input").value;
            const rujukArray = inputJamRujuk.split(":");
            jamRujuk.set('select', [rujukArray[0],rujukArray[1]])

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


        });

        $(document).ready(function() {
            var jamRujuk = $('#jam_rujuk').pickatime().pickatime('picker')
            var inputJamRujuk = document.getElementById("jam_rujuk_input").value;
            const rujukArray = inputJamRujuk.split(":");
            jamRujuk.set('select', [rujukArray[0],rujukArray[1]])


            $('#backBtn').click(function() {
                window.location.href = "{{ route('rujukanMasuk.index') }}";
            });

            $('#saveBtn').click(function(e) {
                e.preventDefault();
                $(this).html('Save');

                swal({
                    title:"Mengirim Surat Balasan",
                    text:"Harap menunggu....",
                    icon: "{{asset('images/logo/loading.gif')}}",
                    buttons: false,
                    closeOnClickOutside: false,
                });

                $.ajax({
                    data: $('#balasanForm').serialize(),
                    url: "{{ route('balasan.store') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        if (data.errors) {
                            swal.close();
                            for(var errorMsg in data.errors) {
                                toastr.error(data.errors[errorMsg], "Gagal membuat surat balasan !");
                            }
                        } else {
                            swal.close();
                            window.location.href = "{{ route('rujukanMasuk.index') }}";
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        $('#saveBtn').html('Save Changes');
                    }
                });

            });


            $("#jam_rujuk").css('pointer-events', 'none');

        });
    </script>
@endpush
